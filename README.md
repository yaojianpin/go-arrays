# go-arrays

#### 介绍
创建一个基于go更好的array操作，就像js的array一样

#### 软件架构
用golang实现一个他javascript的array一样方便的函数式链式调用


#### 安装教程

1.  `go get gitee.com/yaojianpin/go-arrays`

#### 使用说明

```go
arr := []int{8, 5, 6}

// Filter
arrays.From(arr).Filter(func(item int) bool { return item > 5 })

// Count
arrays.From(arr).Count(func(item int) bool { return item > 5 })

// Contains
exists := arrays.From(arr).Contains(5)

// Map
m := arrays.From(arr).Map(func(v int) any { return v * 2 }).Map(func(v int) any { return Tuple{x: v} })

// ForEach 
arrays.From(arr).ForEach(func(v int) { 
  fmt.Println(v)
})

// to slice
m := arrays.From(arr).Slice()

// sort
sortedResult := arrays.From(arr).SortBy(func(i, j int) bool { return i < j })


// Reduce
arr := []int{3, 4, 2}
value := From(arr).Reduce(0, func(acc int, cur int) int {
  return acc + cur
})

// output 9

// Flat
arr := [][]int{{3}, {4}, {3, 5}}

newArr := From(arr).Flat(1).Slice()
for i, v := range newArr {
  fmt.Println(i, v)
}

// output  [ 3, 4, 3, 5 ]


// FlatMap
arr := []string{"it's Sunny in", "California"}

newArr := From(arr).FlatMap(func(item string) any {
  return strings.Split(item, " ")
}, 1).Slice()
// output ["it's", "Sunny", "in", "California" ]

// Take(start, count)
arr := []int{3, 4, 2, 5, 2, 3}
newArr := From(arr).Take(1, 2)

// output [3, 4]

// Find(index)
	arr := []int{3, 4, 2, 5, 2, 3}
	value, err := From(arr).Find(0)
  // output 3,nil

// Distinct
  arr := []int{3, 4, 2, 5, 2, 3}
	From(arr).Distinct(func(item int) string {
		return fmt.Sprint(item)
	}).Slice()
  // output [3,4,2,5]


// Parallel
// parallel to run the fn
	arr := []int{3, 4, 2}
	From(arr).Parallel(func(item int) {
		fmt.Println("Parallel run", item)
	})
	time.Sleep(1 * time.Second)

```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
