package arrays

import (
	"reflect"
)

func Flatten(target *[]any, source any, depth int) {
	if depth < 0 {
		return
	}
	var slice = reflect.ValueOf(source)
	for i := 0; i < slice.Len(); i++ {
		item := slice.Index(i)
		var kind = reflect.TypeOf(item.Interface()).Kind()
		if kind == reflect.Slice || kind == reflect.Array {
			Flatten(target, item.Interface(), depth-1)
		} else {
			*target = append(*target, item)
		}
	}

}
