package arrays

import (
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"
)

type tuple struct{ x, y int }

func Test_Array_From(t *testing.T) {
	arr := []int{3, 4, 2}
	ret := From(arr)
	fmt.Println("Test_Array_From", ret)
}

func Test_Array_Map(t *testing.T) {
	arr := []int{3, 4, 2}
	ret := From(arr).Map(func(i int) any { return tuple{i, i} })
	fmt.Println("Test_Array_Map", ret)
}

func Test_Array_Map_2(t *testing.T) {
	arr := []int{3, 4, 2}
	ret := From(arr).Map(func(i int) any { return i * 2 })
	fmt.Println("Test_Array_Map_2", ret)
}

func Test_Array_Map_Filter(t *testing.T) {
	arr := []int{3, 4, 2}
	ret := From(arr).Map(func(i int) any { return tuple{i, i} }).Filter(func(item any) bool {
		return item.(tuple).x > 3
	})
	fmt.Println("Test_Array_Map_Filter", ret)
}

func Test_Array_Contains(t *testing.T) {
	arr := []int{3, 4, 2}
	var ret = From(arr).Contains(4)
	if !ret {
		t.Errorf("Test_Array_Contains expected true, but got %v", ret)
	}

	ret = From(arr).Contains(10)
	if ret {
		t.Errorf("Test_Array_Contains expected false, but got %v", ret)
	}
}

func Test_Array_FilterInt(t *testing.T) {

	arr := []int{3, 4, 2}
	ret := From(arr).Filter(func(i int) bool { return i > 3 })
	fmt.Println("Test_Array_FilterInt", ret)
}

func Test_Array_FilterStruct(t *testing.T) {
	arr := []tuple{
		{1, 2},
		{2, 3},
		{3, 4},
	}
	ret := From(arr).Filter(func(i tuple) bool { return i.y > 3 })
	fmt.Printf("Test_Array_FilterStruct %v\n", ret)
}

func Test_Array_Count(t *testing.T) {
	arr := []int{3, 4, 2}
	ret := From(arr).Count()
	if ret != 3 {
		t.Errorf("Test_Array_Count expected count=3, but got %v", ret)
	}
}

func Test_Array_Slice(t *testing.T) {
	arr := []string{"banana", "apple", "pear"}
	ret := From(arr).Slice()
	fmt.Println(reflect.TypeOf(ret))
	if reflect.TypeOf([]string{}) != reflect.TypeOf(ret) {
		t.Fail()
	}

}

func Test_Array_Sort(t *testing.T) {

	type tuple struct{ x, y int }
	arr := []int{3, 4, 2}
	ret := From(arr).SortBy(func(i, j int) bool { return i < j }).Slice()
	if ret[0] != 2 {
		t.Errorf("Test_Array_Sort expected ret[0]=2, but got %v\n", ret[0])
	}
}

func Test_Array_SortString(t *testing.T) {

	type tuple struct{ x, y int }
	arr := []string{"banana", "apple", "pear"}
	ret := From(arr).SortBy(func(i, j string) bool { return i < j }).Slice()
	if ret[0] != "apple" {
		t.Errorf("Test_Array_Sort expected ret[0]=apple, but got %v\n", ret[0])
	}
}

func Test_Array_Parallel(t *testing.T) {
	arr := []int{3, 4, 2}
	From(arr).Parallel(func(item int) {
		fmt.Println("Parallel run", item)
	})
	time.Sleep(1 * time.Second)
}

func Test_Array_Distinct(t *testing.T) {
	arr := []int{3, 4, 2, 5, 2, 3}
	count := From(arr).Distinct(func(item int) string {
		return fmt.Sprint(item)
	}).Count()
	if count != 4 {
		t.Errorf("Test_Array_Sort expected count = 4, but got %v\n", count)
	}
}

func Test_Array_Find(t *testing.T) {
	arr := []int{3, 4, 2, 5, 2, 3}
	value, err := From(arr).Find(0)
	if err != nil || value != 3 {
		fmt.Println(value, err)
		t.Fail()
	}
}

func Test_Array_Take(t *testing.T) {
	arr := []int{3, 4, 2, 5, 2, 3}
	newArr := From(arr).Take(1, 2)
	if _, err := newArr.Find(0); err != nil || newArr.Count() != 2 {
		t.Fail()
	}
}

func Test_Array_TakeWithErrorStartAndCount(t *testing.T) {
	arr := []int{3, 4, 2, 5, 2, 3}
	newArr := From(arr).Take(8, 1)
	if newArr.Count() != 0 {
		t.Fail()
	}
}

func Test_Array_Reduce(t *testing.T) {
	arr := []int{3, 4, 2}

	value := From(arr).Reduce(0, func(acc int, cur int) int {
		return acc + cur
	})
	if value != 9 {
		t.Fail()
	}
}

func Test_Array_Flat(t *testing.T) {
	arr := [][]int{{3}, {4}, {3, 5}}

	newArr := From(arr).Flat(1).Slice()
	for i, v := range newArr {
		fmt.Println(i, v)
	}
}

func Test_Array_FlatMap(t *testing.T) {
	arr := []string{"it's Sunny in", "California"}

	newArr := From(arr).FlatMap(func(item string) any {
		return strings.Split(item, " ")
	}, 1).Slice()
	for i, v := range newArr {
		fmt.Println(i, v)
	}
}
